###############################################################################
# add_configuration(ConfigurationName [DEBUG] [BaseConfiguration])
###############################################################################
# Given a BaseConfiguration to copy from, add_configuration will create new
# CMake Cache variables and configuration information for a new configuration.
# The configuration can be modified post add_configuration call.
#
# If no BaseConfiguration is provided, CMake will initialize the cache
# variables to empty strings (By attempting to read invalid strings).
#
# DEBUG can be provided to say that the configuration must link with debug
# runtimes. This is also an optional argument.
###############################################################################
function(add_configuration ConfigurationName)
 
  # Make sure C/CXX are enabled before doing this
  enable_language(C CXX)
 
  # Find our optional flags
  if("${ARGV1}" STREQUAL "DEBUG")
    set(DEBUG_CONFIGURATION TRUE)
    set(BaseConfiguration "${ARGV2}")
  elseif("${ARGV2}" STREQUAL "DEBUG")
    set(DEBUG_CONFIGURATION TRUE)
    set(BaseConfiguration "${ARGV1}")
  else()
    set(BaseConfiguration "${ARGV1}")
  endif()
 
  # Create capital versions
  string(TOUPPER "${ConfigurationName}" CONFIGURATIONNAME)
  string(TOUPPER "${BaseConfiguration}" BASECONFIGURATION)
 
  # Check other potential errors
  if(${ARGC} GREATER 3)
    message(FATAL_ERROR "Malformed configuration detected! Expected max 3 arguments but provided ${ARGC}!")
  endif()
  if(BaseConfiguration AND NOT DEFINED CMAKE_EXE_LINKER_FLAGS_${BASECONFIGURATION})
    message(FATAL_ERROR "Invalid base configuration supplied to add_configuration: ${BaseConfiguration}")
  endif()
 
  # Create the configuration
  list(APPEND CMAKE_CONFIGURATION_TYPES ${ConfigurationName})
  set(CMAKE_C_FLAGS_${CONFIGURATIONNAME} "${CMAKE_C_FLAGS_${BASECONFIGURATION}}" CACHE STRING "" FORCE)
  set(CMAKE_CXX_FLAGS_${CONFIGURATIONNAME} "${CMAKE_CXX_FLAGS_${BASECONFIGURATION}}" CACHE STRING "" FORCE)
  set(CMAKE_EXE_LINKER_FLAGS_${CONFIGURATIONNAME} "${CMAKE_EXE_LINKER_FLAGS_${BASECONFIGURATION}}" CACHE STRING "" FORCE)
  set(CMAKE_SHARED_LINKER_FLAGS_${CONFIGURATIONNAME} "${CMAKE_SHARED_LINKER_FLAGS_${BASECONFIGURATION}}" CACHE STRING "" FORCE)
  set(CMAKE_MODULE_LINKER_FLAGS_${CONFIGURATIONNAME} "${CMAKE_MODULE_LINKER_FLAGS_${BASECONFIGURATION}}" CACHE STRING "" FORCE)
 
  # Register with CMake
  list(APPEND CMAKE_CONFIGURATION_TYPES ${ConfigurationName})
  list(REMOVE_DUPLICATES CMAKE_CONFIGURATION_TYPES)
  set(CMAKE_CONFIGURATION_TYPES "${CMAKE_CONFIGURATION_TYPES}" CACHE STRING "" FORCE)
 
  # Done, print the status
  if(DEBUG_CONFIGURATION)
    get_property(DEBUG_CONFIGURATIONS GLOBAL PROPERTY DEBUG_CONFIGURATIONS)
    list(APPEND DEBUG_CONFIGURATIONS ${ConfigurationName})
    set_property(GLOBAL PROPERTY ${CONFIGURATION} ${DEBUG_CONFIGURATIONS})
    message(STATUS "Added ${ConfigurationName} debug configuration")
  else()
    message(STATUS "Added ${ConfigurationName} configuration")
  endif()
 
endfunction()
 
# Note: We must be sure to initialize valid configurations to the CMake defaults
set(CMAKE_CONFIGURATION_TYPES "Debug;Release")