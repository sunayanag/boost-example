#include(FindPackageHandleStandardArgs)
if (App_USE_Boost)

	set(BOOST_ROOT "" CACHE PATH "Folder contains Boost")
	if (WIN32)
		set(Application_CMAKE_MINIMUM_REQUIRED_VERSION 3.12) # Assuming Boost 1.67
		cmake_minimum_required(VERSION ${Application_CMAKE_MINIMUM_REQUIRED_VERSION})
	endif()

	# Components from boost required
	list(APPEND Application_BOOST_LIBS "filesystem")
	list(APPEND Application_BOOST_LIBS "thread")
	list(APPEND Application_BOOST_LIBS "date_time")
	list(APPEND Application_BOOST_LIBS "system")
	list(APPEND Application_BOOST_LIBS "atomic")
	list(APPEND Application_BOOST_LIBS "chrono")
	# list(APPEND Application_BOOST_LIBS "context")
	# list(APPEND Application_BOOST_LIBS "coroutine")
	# list(APPEND Application_BOOST_LIBS "exception")
	# list(APPEND Application_BOOST_LIBS "graph")
	# list(APPEND Application_BOOST_LIBS "iostreams")
	# list(APPEND Application_BOOST_LIBS "locale")
	# list(APPEND Application_BOOST_LIBS "log")
	# list(APPEND Application_BOOST_LIBS "log_setup")
	# list(APPEND Application_BOOST_LIBS "math_c99")
	# list(APPEND Application_BOOST_LIBS "math_c99f")
	# list(APPEND Application_BOOST_LIBS "math_c99l")
	# list(APPEND Application_BOOST_LIBS "math_tr1")
	# list(APPEND Application_BOOST_LIBS "math_tr1f")
	# list(APPEND Application_BOOST_LIBS "math_tr1l")
	# list(APPEND Application_BOOST_LIBS "prg_exec_monitor")
	# list(APPEND Application_BOOST_LIBS "program_options")
	# list(APPEND Application_BOOST_LIBS "random")
	list(APPEND Application_BOOST_LIBS "unit_test_framework")


	# Sanity checks
	if (DEFINED BOOST_ROOT AND NOT EXISTS ${BOOST_ROOT})
		message(FATAL_ERROR "BOOST_ROOT variable is defined but corresponds to non-existing directory.")
	endif()
	set(BOOST_LIBRARYDIR ${BOOST_ROOT}/lib)
	set(Boost_LIBRARY_DIR_DEBUG ${BOOST_ROOT}/lib)
	set(Boost_LIBRARY_DIR_RELEASE ${BOOST_ROOT}/lib)
	set(BOOST_INCLUDEDIR ${BOOST_ROOT}/include)

	set(Boost_NO_SYSTEM_PATHS ON)   # Notice: This enables us to turn off System Paths.
	set(Boost_NO_BOOST_CMAKE ON)	# Notice: We can tell CMake not to assume this is the CMake-ified version of the boost project.
	if (BUILD_SHARED_LIBS)
		set(Boost_USE_STATIC_LIBS OFF)
		set(Boost_USE_STATIC_RUNTIME OFF)
	else()
		set(Boost_USE_STATIC_LIBS ON)
		set(Boost_USE_STATIC_RUNTIME ON)
	endif()

	find_package(Boost 1.67 EXACT COMPONENTS ${Application_BOOST_LIBS} REQUIRED)
	#Add final configuration
	foreach(_component ${Application_BOOST_LIBS})
  		set_target_properties(Boost::${_component} PROPERTIES MAP_IMPORTED_CONFIG_FINAL "RELEASE")
	endforeach()

	include_directories(${Boost_INCLUDE_DIRS})
	link_directories(${Boost_LIBRARY_DIRS})
	message(STATUS "Boost_INCLUDE_DIRS=${Boost_INCLUDE_DIRS}")
	message(STATUS "Boost_LIBRARY_DIRS=${Boost_LIBRARY_DIRS}")
	list(APPEND ALL_THIRD_PARTY_LIBRARIES ${Boost_LIBRARIES})
endif()