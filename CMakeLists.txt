cmake_minimum_required(VERSION 3.5)

project(boost-example)
list(APPEND CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/CMake)

# Add configurations
include(SetupConfiguration)

# if (WIN32)
#   # Restrict the generated configuration to be what we configured above.
#   # No point creating project files for build types that will not compile.
#   # Note: it's set to FORCE so that both CMAKE_BUILD_TYPE and CMAKE_CONFIGURATION_TYPES match up.
#   set(CMAKE_CONFIGURATION_TYPES ${CMAKE_BUILD_TYPE} CACHE STRING "Build configurations to generate." FORCE)
#   mark_as_advanced(CMAKE_CONFIGURATION_TYPES)
# endif()
message("Generated with config types: ${CMAKE_CONFIGURATION_TYPES}")
 
######################################################################
# Choose C++ standard. Currently 11, as we try to support VS2013.
######################################################################
set(mp_CXX_STANDARD 11)
set(CMAKE_CXX_EXTENSIONS 0)
set(CMAKE_CXX_STANDARD ${mp_CXX_STANDARD})
set(CMAKE_CXX_STANDARD_REQUIRED ON)

######################################################################
# Add Optional Requirements
######################################################################
if(WIN32)
  set(_library_sub_dir "bin")
else()
  set(_library_sub_dir "lib")
endif()

## Boost
option(App_USE_Boost "Use Boost." ON)
include(mpIncludeBoost)

add_executable(boost-example main.cpp)
if (CMAKE_CONFIGURATION_TYPES MATCHES Release)
  target_link_libraries(boost-example ${Boost_LIBRARIES})
  set_property(TARGET boost-example PROPERTY MSVC_RUNTIME_LIBRARY "MultiThreaded$<$<CONFIG:Release>:Release>")
  set_target_properties(boost-example PROPERTIES RELEASE_POSTFIX "R")
elseif(CMAKE_CONFIGURATION_TYPES MATCHES Final)
  target_link_libraries(boost-example ${Boost_LIBRARIES})
  set_property(TARGET boost-example PROPERTY MSVC_RUNTIME_LIBRARY "MultiThreaded$<$<CONFIG:Final>:Final>")
  set_target_properties(boost-example PROPERTIES FINAL_POSTFIX "F")
  add_definitions(-DFINAL)
elseif(CMAKE_CONFIGURATION_TYPES MATCHES Debug)
  target_link_libraries(boost-example ${Boost_LIBRARIES})
  set_property(TARGET boost-example PROPERTY MSVC_RUNTIME_LIBRARY "MultiThreaded$<$<CONFIG:Debug>:Debug")
  set_target_properties(boost-example PROPERTIES DEBUG_POSTFIX "D")
endif()